<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('producto_id');
            $table->integer('users_id');
            $table->integer('cantidad');
            $table->text('detalle'); //"nombre_usuario" agrego "cantidad" producto(s) al inventario
            $table->text('referencia')->nullable();
            $table->string('fecha');
            $table->string('hora');
            $table->string('tipo'); //PUEDE ENTRADA O SALIDA

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventarios');
    }
}
