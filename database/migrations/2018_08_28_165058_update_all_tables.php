<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->string('user_id')->nullable();
        });

        Schema::table('productos', function (Blueprint $table) {
            $table->string('user_id')->nullable();
        });

        Schema::table('inventarios', function (Blueprint $table) {
            $table->string('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

        Schema::table('productos', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

        Schema::table('inventarios', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
