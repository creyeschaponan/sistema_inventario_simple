<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        \App\User::create([
            'username' => 'adminkh01',
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' =>bcrypt('clave2018kh1')
        ]);
//        \App\User::create([
//            'username' => 'admin2',
//            'name' => 'admin2',
//            'email' => 'admin2@gmail.com',
//            'password' =>bcrypt('admin123')
//        ]);
//        \App\User::create([
//            'username' => 'admin3',
//            'name' => 'admin3',
//            'email' => 'admin3@gmail.com',
//            'password' =>bcrypt('admin123')
//        ]);
//        \App\User::create([
//            'username' => 'admin4',
//            'name' => 'admin4',
//            'email' => 'admin4@gmail.com',
//            'password' =>bcrypt('admin123')
//        ]);
    }
}
