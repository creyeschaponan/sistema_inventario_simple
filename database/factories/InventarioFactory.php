<?php

use Faker\Generator as Faker;

$factory->define(\App\Inventario::class, function (Faker $faker) {
    $cantidad = $faker->randomNumber(2);
    $tipo = $faker->randomElement(['ENTRADA','SALIDA']);
    return [
        'producto_id' => function(){
            return factory(\App\Producto::class)->create()->id;
        },
        'users_id' => '1',
        'cantidad' => $cantidad,
        'detalle' =>
           $tipo == 'ENTRADA' ? 'El usuario agrego' . $cantidad . ' productos(s) al inventario': 'El usuario saco' . $cantidad . ' productos(s) del inventario'
           ,
        'referencia' => $faker->postcode,
        'fecha' => $faker->date('d/m/Y','now'),
        'hora' => $faker->time('H:m:s','now'),
        'tipo' => $tipo
    ];
});


