<?php

use Faker\Generator as Faker;

$factory->define(\App\Producto::class, function (Faker $faker) {
    return [
        'codigo' => $faker->postcode,
        'nombre' => $faker->name,
        'categoria_id' => function(){
            return factory(\App\Categoria::class)->create()->id;
        },
        'precio' => $faker->randomFloat(2,1,1000),
        'url_imagen' => $faker->imageUrl()
    ];
});
