<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Producto extends Model
{

    use SoftDeletes;

    const ESTADO_ACTIVO='AC';
    const ESTADO_INACTIVO='IN';

    protected $table="productos";

    public function user()
    {
        return $this->belongsTo('App\User')->withDefault();
    }

    public function scopeCargarListado($query,$parametro){

        $query->select('productos.id','productos.codigo','productos.nombre','productos.url_imagen','productos.user_id','productos.estado');
        $query->join('categorias','productos.categoria_id','=','categorias.id');
        if($parametro != null){
            if(array_key_exists('codigo_nombre',$parametro)){
                if( $parametro['codigo_nombre'] != null){
                    $query->where(function($subquery) use ($parametro){
                        $subquery->orWhere('productos.codigo','like', '%' . $parametro['codigo_nombre'] . '%');
                        $subquery->orWhere('productos.nombre','like', '%' . $parametro['codigo_nombre'] . '%');
                    });
                }
            }elseif(array_key_exists('categoria',$parametro)){
                if($parametro['categoria'] != null){
                    $query->where(function($subquery) use ($parametro){
                        $subquery->orWhere('categoria.nombre','like', '%' . $parametro['categoria'] . '%');
                    });
                }
            }
        }

        return $query;
    }

    public function crearPrimeraEntrada($request)
    {
        $inventario = new Inventario();
        $inventario->producto_id = $this->id;
        $inventario->users_id = Auth::id();
        $inventario->cantidad = $request->get('stock');
        $inventario->detalle =  ''. Auth::user()->name . " agrego: " . $request->get('stock') . ' producto(s) al inventario';

        $fecha = Carbon::now();
        $fecha = $fecha->format('d/m/Y');

        $hora = Carbon::now();
        $hora = $hora->format('H:m:s');

        $inventario->fecha = $fecha;
        $inventario->hora = $hora;
        $inventario->tipo = 'ENTRADA';

        $inventario->save();
    }
    
    public function cantidadInventario()
    {
        $total_entradas = Inventario::calcularEntradas($this->id);
        $total_salidas = Inventario::calcularSalidas($this->id);

        $total = $total_entradas - $total_salidas;

        return $total;
    }

    public function nombreCategoria()
    {
        $categoria = Categoria::findOrFail($this->categoria_id);
        return $categoria->nombre;
    }


}
