<?php

namespace App\Http\Controllers;

use App\User;
use App\Util\Notificacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{
    public function listado()
    {

        $listado_usuarios = User::orderBy('id','DESC')->get();
        foreach ($listado_usuarios as $usuario) {
            $usuario->url_eliminar = route('admin.usuarios.eliminar',$usuario->id);
            $usuario->url_editar = route('admin.usuarios.editar',$usuario->id);
        }

        $data = compact('listado_usuarios');
        return view('admin.usuarios.listado',$data);
    }

    public function crear()
    {
        $usuario = new User();
        $data = compact('usuario');
        return view('admin.usuarios.formulario',$data);
    }

    public function almacenar(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|min:3',
            'username' => 'required|min:3',
            'password' => 'required|confirmed|min:6|max:18',
            'estado' => 'required'
        ],[
            'password.required' => 'El campo es requerido',
            'password.confirmed' => 'Los passwords no coinciden',
            'password.min' => 'El mínimo permitido son 6 caracteres',
            'password.max' => 'El máximo permitido son 18 caracteres',
        ]);
        try{
            $usuario = new User();
            $usuario->name = $request->input('name');
            $usuario->username = $request->input('username');
            $usuario->password = bcrypt($request->input('password'));
            $usuario->estado = $request->input('estado');

            DB::transaction(function() use ($usuario){
                $usuario->save();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "El usuario " . $request->input('nombre') ." se guardó correctamente.");
            return redirect()->route('admin.usuarios.listado')->with('notificacion',$notificacion);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function editar($id)
    {
        try{
            $usuario = User::findOrfail($id);
            $data = compact('usuario');
            return view('admin.usuarios.formulario',$data);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function actualizar(Request $request,$id)
    {
        $this->validate($request,[
            'name' => 'required|min:3',
            'username' => 'required|min:3',
            'password' => 'required|confirmed|min:6|max:18',
            'estado' => 'required'
        ],[
            'password.required' => 'El campo es requerido',
            'password.confirmed' => 'Los passwords no coinciden',
            'password.min' => 'El mínimo permitido son 6 caracteres',
            'password.max' => 'El máximo permitido son 18 caracteres',
        ]);
        try{
            $usuario = User::findOrFail($id);
            $usuario->name = $request->input('name');
            $usuario->username = $request->input('username');
            $usuario->password = bcrypt($request->input('password'));
            $usuario->estado = $request->input('estado');

            DB::transaction(function() use ($usuario){
                $usuario->save();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "El usuario " . $request->input('nombre') ." se actualizó correctamente.");
            return redirect()->route('admin.usuarios.listado')->with('notificacion',$notificacion);

        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function eliminar($id)
    {
        try{
            $usuario = User::findOrFail($id);
            $nombre = $usuario->name;
            DB::transaction(function() use ($usuario){
                $usuario->delete();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "El usuario " . $nombre ." se elimino correctamente.");
            return redirect()->route('admin.usuarios.listado')->with('notificacion',$notificacion);
        }catch(Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }
}
