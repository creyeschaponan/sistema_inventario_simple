<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Inventario;
use App\Producto;
use App\Util\Notificacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProductoController extends Controller
{
    public function crear()
    {
        $listado_categorias = Categoria::where('estado','=','AC')->get();
        $producto = new Producto();
        $data = compact('producto','listado_categorias');
        return view('admin.producto.formulario',$data);
    }

    public function almacenar(Request $request)
    {
        $this->validate($request,[
//            'imagen' => 'required|image',
            'codigo' => 'required',
            'nombre' => 'required',
            'categoria_id' => 'required',
            'precio' => 'required',
            'stock' => 'required|numeric',
            'estado' => 'required'
        ]);

        try{
            $producto = new Producto();
            $producto->codigo = strtoupper($request->input('codigo'));
            $producto->nombre = strtoupper($request->input('nombre'));
            $producto->categoria_id = $request->input('categoria_id');
            $producto->precio = $request->input('precio');
            $producto->estado = $request->input('estado');
            $producto->user_id = Auth::user()->id;

//            $imagen = $request->file('imagen');
//            $nombre = $imagen->getClientOriginalName();

            DB::transaction(function() use ($producto,$request){
//                Storage::disk('publico')->put($nombre,File::get($imagen));
//                $producto->url_imagen = $nombre;
                $producto->save();
                $producto->crearPrimeraEntrada($request);
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "El producto " . $request->input('nombre') ." se creo correctamente");
            return back()->with('notificacion',$notificacion);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, "No se pudo crear el producto " . $request->input('nombre') ." correctamente - Error: " . $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function detalle($id)
    {
        try{
            $producto = Producto::findOrFail($id);
            $producto->url_editar = route('admin.productos.editar',$producto->id);
            $producto->url_eliminar = route('admin.productos.eliminar',$producto->id);
            $producto->url_cambiarImagen = route('admin.producto.cambiar.imagen',$producto->id);

            $listado_inventario = Inventario::encontrarInventario($id)->get();

            $data = compact('producto','listado_inventario');
            return view('admin.producto.detalle',$data);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function listadoAjax($id)
    {
        $listado_inventario = Inventario::encontrarInventario($id)->get();
        foreach ($listado_inventario as $inventario) {
            $inventario->fecha_hora = $inventario->fecha . ' - ' . $inventario->hora;
        }
        return datatables()->collection($listado_inventario)
            ->setRowClass('{{ $tipo == "ENTRADA" ? "table-success" : "table-danger" }}')
            ->toJson();
    }

    public function cambiarImagen(Request $request,$id)
    {
        $this->validate($request,[
            'imagen' => 'required|image'
        ]);

        try{
            if($request->file('imagen') != null){
                $imagen = $request->file('imagen');
                $nombre = $imagen->getClientOriginalName();

                DB::transaction(function() use ($nombre,$imagen,$id){
                    $producto = Producto::findOrFail($id);
                    $producto->url_imagen = $nombre;
                    $producto->save();
                    Storage::disk('publico')->put($nombre,File::get($imagen));
                });
                $notificacion=new Notificacion(Notificacion::EXITO, "La imagen se subio correctamente");
                return redirect()->back()->with('notificacion',$notificacion);
            }else{
                $notificacion=new Notificacion(Notificacion::ALERTA, "Error: " .  'No ha ingresado ninguna imagen');
                return back()->with('notificacion',$notificacion);
            }

        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, "Error: " .  $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function editar($id)
    {
        try{
            $producto = Producto::findOrFail($id);
            $listado_categorias = Categoria::where('estado','=','AC')->get();
            $data = compact('producto','listado_categorias');
            return view('admin.producto.formulario',$data);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function actualizar(Request $request,$id)
    {
        $this->validate($request,[
            'codigo' => 'required',
            'nombre' => 'required',
            'categoria_id' => 'required',
            'precio' => 'required',
            'estado' => 'required'
        ]);

        try{
            $producto = Producto::findOrFail($id);
            $producto->codigo = strtoupper($request->input('codigo'));
            $producto->nombre = strtoupper($request->input('nombre'));
            $producto->categoria_id = $request->input('categoria_id');
            $producto->precio = $request->input('precio');
            $producto->estado = $request->input('estado');
            $producto->user_id = Auth::user()->id;

            DB::transaction(function() use ($producto){
                $producto->save();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "El producto " . $request->input('nombre') ." se actualizó correctamente");
            return redirect()->route('admin.productos.detalle',$id)->with('notificacion',$notificacion);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, "No se pudo actualizar el producto " . $request->input('nombre') ." correctamente - Error: " . $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function eliminar($id)
    {
        try{
            $producto = Producto::findOrFail($id);
            $nombre = $producto->nombre;
            $ruta_restaurar = route('admin.producto.restaurar',$id);

            DB::transaction(function() use ($producto){
                $producto->delete();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "El producto " . $nombre ." se eliminó correctamente. Si desea deshacer la eliminacion: "
                    ,true ,$ruta_restaurar);
            return redirect()->route('admin.inventario.listado')->with('notificacion',$notificacion);
        }catch(Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function restaurar($id)
    {
        try{
            $producto = Producto::withTrashed()->where('id', '=', $id)->first();
            $nombre = $producto->nombre;
            //Restauramos el registro
            $producto->restore();

            $notificacion=new Notificacion(Notificacion::EXITO, "El producto " . $nombre ." se restauro correctamente");
            return redirect()->route('admin.productos.detalle',$id)->with('notificacion',$notificacion);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }
}
