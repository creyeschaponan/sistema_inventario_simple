<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Util\Notificacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoriaController extends Controller
{
    public function listado()
    {

        $listado_categorias = Categoria::orderBy('id','DESC')->get();
        foreach ($listado_categorias as $categoria) {
            $categoria->url_eliminar = route('admin.categoria.eliminar',$categoria->id);
            $categoria->url_editar = route('admin.categoria.editar',$categoria->id);
        }

        $data = compact('listado_categorias');
        return view('admin.categoria.listado',$data);
    }

    public function listadoAjax()
    {
        $listado_categorias = Categoria::orderBy('id','DESC')->get();
        foreach ($listado_categorias as $categoria) {
            $categoria->usuario = $categoria->user->name ." / " .  $categoria->user->username;
            $categoria->url_eliminar = route('admin.categoria.eliminar',$categoria->id);
            $categoria->url_editar = route('admin.categoria.editar',$categoria->id);
        }
        return datatables()->collection($listado_categorias)
            ->addColumn('estado', function (Categoria $modelo){
                $data = compact('modelo');
                return view('layouts.estado',$data);
            })
            ->addColumn('opciones',function (Categoria $modelo){
                $data = compact('modelo');
                return view('layouts.opciones',$data);
            })
            ->rawColumns(['estado','opciones'])
            ->toJson();
    }

    public function crear()
    {
        $categoria = new Categoria();
        $data = compact('categoria');
        return view('admin.categoria.formulario',$data);
    }

    public function almacenar(Request $request)
    {
        $this->validate($request,[
            'nombre' => 'required|min:3',
            'descripcion' => 'required|min:3',
            'estado' => 'required'
        ]);
        try{
            $categoria = new Categoria();
            $categoria->nombre = strtoupper($request->input('nombre'));
            $categoria->descripcion = strtoupper($request->input('descripcion'));
            $categoria->estado = $request->input('estado');
            $categoria->user_id = Auth::user()->id;
            DB::transaction(function() use ($categoria){
                $categoria->save();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "La categoria " . $request->input('nombre') ." se guardo correctamente.");
            return redirect()->route('admin.categoria.listado')->with('notificacion',$notificacion);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function editar($id)
    {
        try{
            $categoria = Categoria::findOrfail($id);
            $data = compact('categoria');
            return view('admin.categoria.formulario',$data);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function actualizar(Request $request,$id)
    {
        $this->validate($request,[
            'nombre' => 'required|min:3',
            'descripcion' => 'required|min:3',
            'estado' => 'required'
        ]);
        try{
            $categoria = Categoria::findOrFail($id);
            $categoria->nombre = strtoupper($request->input('nombre'));
            $categoria->descripcion = strtoupper($request->input('descripcion'));
            $categoria->estado = $request->input('estado');
            $categoria->user_id = Auth::user()->id;

            DB::transaction(function() use ($categoria){
                $categoria->save();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "La categoria " . $request->input('nombre') ." se actualizo correctamente.");
            return redirect()->route('admin.categoria.listado')->with('notificacion',$notificacion);

        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function eliminar($id)
    {
        try{
            $categoria = Categoria::findOrFail($id);
            $nombre = $categoria->nombre;
            DB::transaction(function() use ($categoria){
                $categoria->delete();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "La Categoría " . $nombre ." se elimino correctamente.");
            return redirect()->route('admin.categoria.listado')->with('notificacion',$notificacion);
        }catch(Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }
}
