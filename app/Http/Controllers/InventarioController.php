<?php

namespace App\Http\Controllers;

use App\Inventario;
use App\Producto;
use App\Util\Notificacion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InventarioController extends Controller
{
    public function listado(Request $request)
    {
        $params_get=$request->only(['codigo_nombre','categoria']);
        $listado_productos = Producto::cargarListado($params_get)->with('user')->paginate(8);

        $data = compact('listado_productos');
        return view('admin.inventario.listado',$data);
    }

    public function agregar($id)
    {
        $producto = Producto::findOrFail($id);
        $inventario = new Inventario();
        $estado = "entrada";
        $data = compact('producto','inventario','estado');

        return view('admin.inventario.formulario',$data);
    }

    public function almacenar(Request $request,$id)
    {
        $this->validate($request,[
            'cantidad' => 'required|numeric',
            //'referencia' => 'required|min:3',
            'fecha' => 'required',
            //'numero_recibo' => 'required|numeric'
        ]);

        try{
            $inventario = new Inventario();
            $inventario->producto_id = $id;
            $inventario->users_id = Auth::id();
            $inventario->cantidad = $request->input('cantidad');
            $inventario->detalle =  ''. Auth::user()->name . " agrego: " . $request->input('cantidad') . ' producto(s) al inventario';
            $inventario->referencia = strtoupper($request->input('referencia'));

            $fecha_editable = new Carbon($request->input('fecha'));
            $fh_edit = $fecha_editable->format('d/m/Y');

            $inventario->fecha_editable = $fh_edit;

            $inventario->numero_recibo = $request->input('numero_recibo');

            $fecha = Carbon::now();
            $fecha = $fecha->format('d/m/Y');

            $hora = Carbon::now();
            $hora = $hora->format('H:m:s');

            $inventario->fecha = $fecha;
            $inventario->hora = $hora;

            $inventario->tipo = 'ENTRADA';

            DB::transaction(function() use ($inventario){
                $inventario->save();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "El stock se agregó correctamente");
            return redirect()->route('admin.productos.detalle',$id)->with('notificacion',$notificacion);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, "No se pudo agregar el stock correctamente - Error: " . $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function sacar($id)
    {
        $producto = Producto::findOrFail($id);
        $inventario = new Inventario();
        $estado = "salida";
        $data = compact('producto','inventario','estado');

        return view('admin.inventario.formulario',$data);
    }

    public function actualizar(Request $request,$id)
    {
        $this->validate($request,[
            'cantidad' => 'required|numeric',
            //'referencia' => 'required|min:3',
            'fecha' => 'required',
            //'numero_recibo' => 'required|numeric'
        ]);

        try{
            $inventario = new Inventario();
            $inventario->producto_id = $id;
            $inventario->users_id = Auth::id();
            $inventario->cantidad = $request->input('cantidad');
            $inventario->detalle =  ''. Auth::user()->name . " saco: " . $request->input('cantidad') . ' producto(s) al inventario';
            $inventario->referencia = strtoupper($request->input('referencia'));

            $fecha_editable = new Carbon($request->input('fecha'));
            $fh_edit = $fecha_editable->format('d/m/Y');
            $inventario->fecha_editable = $fh_edit;

            $inventario->numero_recibo = $request->input('numero_recibo');

            $fecha = Carbon::now();
            $fecha = $fecha->format('d/m/Y');

            $hora = Carbon::now();
            $hora = $hora->format('H:m:s');

            $inventario->fecha = $fecha;
            $inventario->hora = $hora;

            $inventario->tipo = 'SALIDA';

            DB::transaction(function() use ($inventario){
                $inventario->save();
            });

            $notificacion=new Notificacion(Notificacion::EXITO, "Salida de stock con éxito");
            return redirect()->route('admin.productos.detalle',$id)->with('notificacion',$notificacion);
        }catch (\Exception $e){
            $notificacion=new Notificacion(Notificacion::ALERTA, "No se pudo sacar el stock correctamente - Error: " . $e->getMessage());
            return back()->with('notificacion',$notificacion);
        }
    }

    public function eliminar($id)
    {

    }
}
