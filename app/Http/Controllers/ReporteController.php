<?php

namespace App\Http\Controllers;

use App\Inventario;
use App\Producto;
use Illuminate\Http\Request;

class ReporteController extends Controller
{
    public function listado()
    {
        $listado_inventario = Producto::all();
        $data = compact('listado_inventario');
        return view('admin.reporte.listado',$data);
    }

    public function listadoAjax()
    {
        $listado_inventario = Producto::all();
        foreach ($listado_inventario as $producto) {
            $producto->categoria = $producto->nombreCategoria();
            $producto->stock =  $producto->cantidadInventario();
        }
        return datatables()->collection($listado_inventario)
                           ->toJson();
    }
}
