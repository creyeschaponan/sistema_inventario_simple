<?php

namespace App\Util;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    //
    const EXITO=1;
    const ADVERTENCIA=2;
    const ALERTA=3;
    const ERROR=4;
    const CARGANDO=5;

    public function __construct($id,$mensaje, $restaurar  = false,$url = null){
        $this->id=$id;
        switch($id){
            case self::EXITO:
                $this->clase='alert-success';
                $this->tipo='¡ÉXITO!';
                break;
            case self::ADVERTENCIA:
                $this->clase='alert-warning';
                $this->tipo='¡ADVERTENCIA!';
                break;
            case self::ALERTA:
                $this->clase='alert-danger';
                $this->tipo='¡ERROR!';
                break;
            default:
                $this->clase='alert-danger';
                $this->tipo="No definido";
                break;
        }

        $this->mensaje=$mensaje;
        $this->restaurar=$restaurar;
        $this->url = $url;
    }

    public function __toString(){
        return $this->mensaje;
    }

}
