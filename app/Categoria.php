<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Categoria extends Model
{

    use SoftDeletes;

    const ESTADO_ACTIVO='AC';
    const ESTADO_INACTIVO='IN';

    protected $table="categorias";


    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function scopeFiltrarCategorias($query,$parametro){
        $query->select('*');
        if($parametro != null){
            $query->where(function($subquery) use ($parametro){
                $subquery->orWhere('param','like', '%' . $parametro . '%');
                $subquery->orWhere('param','like', '%' . $parametro . '%');
            });
        }
        return $query;
    }

}
