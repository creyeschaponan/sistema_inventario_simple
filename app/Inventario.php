<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventario extends Model
{

    use SoftDeletes;

    const ESTADO_ACTIVO='AC';
    const ESTADO_INACTIVO='IN';

    protected $table="inventarios";
    public function scopeFiltrarModulo($query,$parametro){
        $query->select('*');
        if($parametro != null){
            $query->where(function($subquery) use ($parametro){
                $subquery->orWhere('param','like', '%' . $parametro . '%');
                $subquery->orWhere('param','like', '%' . $parametro . '%');
            });
        }
        return $query;
    }

    public function scopeEncontrarInventario($query,$id_producto){
        $query->select('*');
        $query->join('productos','inventarios.producto_id','productos.id');
        $query->where('productos.id','=',$id_producto);

        return $query;
    }



    public static function calcularEntradas($id_producto)
    {
        $listado_entradas = Inventario::where('producto_id','=',$id_producto)->where('tipo','=','ENTRADA')->get();
        $total = 0;
        foreach ($listado_entradas as $entrada){
            $total = $total + $entrada->cantidad;
        }
        return $total;

    }

    public static function calcularSalidas($id_producto)
    {
        $listado_salidas = Inventario::where('producto_id','=',$id_producto)->where('tipo','=','SALIDA')->get();
        $total = 0;
        foreach ($listado_salidas as $salida){
            $total = $total + $salida->cantidad;
        }
        return $total;
    }
}
