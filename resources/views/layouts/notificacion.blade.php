@if(session('notificacion'))
    <div class="x_content bs-example-popovers">
        <div class="alert {{ session('notificacion')->clase }} alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ session('notificacion')->tipo }}</strong> {{ session('notificacion')->mensaje }}

            @if(session('notificacion')->restaurar != false )
                ¿Desea restaurar el producto? <a href="{{ session('notificacion')->url }}">Haga click aquí</a>
            @endif
        </div>
    </div>
@endif
