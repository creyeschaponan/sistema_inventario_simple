<div class="text-center">
    @if($modelo->estado==$modelo::ESTADO_ACTIVO)
        <a href="#" class="btn btn-success btn-sm">ACTIVO</a>
    @elseif($modelo->estado==$modelo::ESTADO_INACTIVO)
        <a href="#" class="btn btn-danger btn-sm">INACTIVO</a>
    @endif
</div>