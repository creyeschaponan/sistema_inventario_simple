<form action="{{ $modelo->url_eliminar }}" method="post" class=" inline">
    @csrf
    {!! method_field('DELETE') !!}
    <div class="btn-group btn-group-sm">
        <a href="{{ $modelo->url_editar}}" class="btn btn-warning">
            <span class="icon-pencil2"></span> Editar
        </a>
        <button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar '{{ $modelo->nombre }}' ?">
            <span class="icon-bin"></span> Eliminar
        </button>
    </div>
</form>