@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.modal')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @include('layouts.notificacion')
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="{{ route('admin.productos.detalle',$producto->id) }}" class="btn btn-primary">Regresar</a>
                            </div>
                            <div class="col-md-10">
                                <h3>{{ $estado == 'entrada' ? 'Agregar' : 'Sacar' }} Inventario: <span class="text-success">{{ $producto->nombre }}</span></h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="" class="form-horizontal form-label-left" accept-charset="UTF-8" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row ">
                                <label for="cantidad" class="col-md-4 col-form-label text-md-right">Cantidad <span class="required">*</span></label>

                                <div class="col-md-6">
                                    <input id="cantidad" type="number" class="form-control{{ $errors->has('cantidad') ? ' is-invalid' : '' }} "
                                           name="cantidad" value="{{ old('cantidad',$inventario->cantidad) }}"
                                           >
                                    @if ($errors->has('cantidad'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('cantidad') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="referencia" class="col-md-4 col-form-label text-md-right">Referencia</label>

                                <div class="col-md-6">
                                    <input id="referencia" type="text" maxlength="30" class="form-control{{ $errors->has('referencia') ? ' is-invalid' : '' }} "
                                           name="referencia" value="{{ old('referencia',$inventario->referencia) }}"
                                            autofocus>
                                    @if ($errors->has('referencia'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('referencia') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="fecha" class="col-md-4 col-form-label text-md-right">Fecha <span class="required">*</span></label>

                                <div class="col-md-6">
                                    <input id="fecha" type="date" class="form-control{{ $errors->has('fecha') ? ' is-invalid' : '' }} "
                                           name="fecha" value="{{ old('fecha',$inventario->fecha) != '' ? old('fecha_nacimiento',$inventario->fecha) : date('Y') . '-'. date('m') .'-' .date('d')  }}"
                                    >@if ($errors->has('fecha'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('fecha') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="numero_recibo" class="col-md-4 col-form-label text-md-right"># de Recibo</label>

                                <div class="col-md-6">
                                    <input id="numero_recibo" type="number" class="form-control{{ $errors->has('numero_recibo') ? ' is-invalid' : '' }} "
                                           name="numero_recibo" value="{{ old('numero_recibo',$inventario->numero_recibo) }}"
                                    >
                                    @if ($errors->has('numero_recibo'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('numero_recibo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-5">
                                    <button id="send" type="submit" class="btn btn-success">
                                        @if($estado == 'entrada')
                                            Agregar
                                        @else
                                            Sacar
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection