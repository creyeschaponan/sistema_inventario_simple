@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.modal')
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('layouts.notificacion')
                <div class="card">
                    <div class="card-header text-white bg-success">
                        <div class="row">
                            <div class="col-md-6">
                                <h3><span class="icon-search"></span> Consultar Inventario</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{ route('admin.productos.crear') }}" class="btn btn-primary"><span class="icon-plus"></span> Agregar</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="text-dark font-weight-bold">Filtrar por código o nombre</p>
                                        <form action="" class="form-horizontal" style="">
                                            <div class="input-group ">
                                                <input type="text" name="codigo_nombre" id="codigo_nombre" class="form-control" aria-label="..." value="{{ app('request')->get('codigo_nombre') }}"
                                                       placeholder="Escriba un código o nombre del producto">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="text-dark font-weight-bold">Filtrar por categoría</p>
                                        <form action="" class="form-horizontal" style="">
                                            <div class="input-group ">
                                                <input type="text" name="categoria" id="categoria" class="form-control" aria-label="..." value="{{ app('request')->get('categoria') }}"
                                                       placeholder="Escriba una categoría">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <br>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                            <table class="table table-striped table-bordered dt-responsive nowrap">
                                                <thead>
                                                <tr>
                                                    <th>Código</th>
                                                    <th>Nombres / Usuario</th>
                                                    <th>Nombre</th>
                                                    <th>Cantidad (Inventario)</th>
                                                    <th>Estado</th>
                                                    <th>Opciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($listado_productos) && count($listado_productos)>0)
                                                    @foreach($listado_productos as $id=>$producto)
                                                        <tr>
                                                            <td>{{ $producto->codigo }}</td>
                                                            <td>{{ $producto->user->name }} ({{ $producto->user->username }})</td>
                                                            <td>{{ $producto->nombre }}</td>
                                                            <td>{{ $producto->cantidadInventario()  }}</td>
                                                            <td>
                                                                @if($producto->estado==$producto::ESTADO_ACTIVO)
                                                                    <a href="#" class="btn btn-success btn-sm">ACTIVO</a>
                                                                @elseif($producto->estado==$producto::ESTADO_INACTIVO)
                                                                    <a href="#" class="btn btn-danger btn-sm">INACTIVO</a>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-sm btn-primary" href="{{ route('admin.productos.detalle',$producto->id) }}">
                                                                    Ver
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="6"> <h4>No hay productos registrados</h4></td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="6"> {{ $listado_productos->links() }}</td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
