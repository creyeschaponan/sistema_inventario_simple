@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.modal')
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('layouts.notificacion')
                <div class="card">
                    <div class="card-header text-white bg-success">
                        <div class="row">
                            <div class="col-md-6">
                                <h3><span class="icon-search"></span> Buscar Productos</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="datatable_productos_reporte" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Categoría</th>
                                <th>Precio</th>
                                <th>Stock</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--@if(isset($listado_inventario) && count($listado_inventario)>0)--}}
                                {{--@foreach($listado_inventario as $id=>$inventario)--}}
                                    {{--<tr>--}}
                                        {{--<td>{{ $inventario->codigo }}</td>--}}
                                        {{--<td>{{ $inventario->nombre }}</td>--}}
                                        {{--<td>{{ $inventario->nombreCategoria() }}</td>--}}
                                        {{--<td>{{ $inventario->precio }}</td>--}}
                                        {{--<td>{{ $inventario->cantidadInventario() }}</td>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                            {{--@endif--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready( function () {
            $(".alert").fadeTo(500, 500).delay(5000).slideUp(500);
            $('#datatable_productos_reporte').dataTable({
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },
                serverSide: true,
                ajax: "{{ route('admin.reportes.listado.ajax') }}",
                columns: [
                    {data: 'codigo'},
                    {data: 'nombre'},
                    {data: 'categoria'},
                    {data: 'precio'},
                    {data: 'stock'},
                ],
                order: [[1, 'asc']],
                dom: 'lBfrtip',
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10', '25', '50', 'Todos' ]
                ],
                buttons: [{
                    extend: 'excel',
                    text: 'Exporta a Excel',
                    exportOptions:{
                        modifier : {
                            order : 'applied',  // 'current', 'applied', 'index',  'original'
                            page : 'all',      // 'all',     'current'
                            search : 'applied'     // 'none',    'applied', 'removed'
                        }
                    }
                }]
            });
        } );
    </script>
@endpush