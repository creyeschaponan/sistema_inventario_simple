@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.modal')
        <div class="row justify-content-center">
                <div class="col-md-12">
                    @include('layouts.notificacion')
                </div>
                <div class="card row">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="col-md-12">--}}
                                            {{--<form action="{{ $producto->url_cambiarImagen }}" method="POST" class="form-horizontal form-label-left" accept-charset="UTF-8" enctype="multipart/form-data">--}}
                                                {{--@csrf--}}
                                                {{--<div class="form-group row ">--}}
                                                    {{--<label for="stock" class="col-md-4 col-form-label text-md-center">Imagen</label>--}}

                                                    {{--<div class="col-md-8">--}}
                                                        {{--<input id="imagen" type="file" name="imagen" class="form-control{{ $errors->has('imagen') ? ' is-invalid' : '' }} ">--}}
                                                        {{--@if ($errors->has('imagen'))--}}
                                                            {{--<span class="invalid-feedback text-danger">--}}
                                                                {{--<strong>{{ $errors->first('imagen') }}</strong>--}}
                                                            {{--</span>--}}
                                                        {{--@endif--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group row mb-0">--}}
                                                    {{--<div class="col-md-6 offset-md-3">--}}
                                                        {{--<button id="send" type="submit" class="btn btn-success">--}}
                                                            {{--<span class="icon-folder-upload"></span> Subir imagen--}}
                                                        {{--</button>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-12 text-center">--}}
                                            {{--<br>--}}
                                            {{--<img class="card-img-top img-thumbnail"--}}
                                                 {{--alt="{{ $producto->nombre }}"--}}
                                                 {{--style="width:70%;"--}}
                                                 {{--src="{{ asset('images/'. $producto->url_imagen)  }}" data-holder-rendered="true">--}}
                                            {{--<br>--}}
                                            {{--<br>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-12 text-center">--}}
                                            {{--<form action="{{ $producto->url_eliminar }}" method="post" class=" inline">--}}
                                                {{--@csrf--}}
                                                {{--{!! method_field('DELETE') !!}--}}
                                                {{--<div class="btn-group btn-group-md">--}}
                                                    {{--<a href="{{ $producto->url_editar}}" class="btn btn-warning">--}}
                                                        {{--<span class="icon-pencil2"></span> Editar--}}
                                                    {{--</a>--}}
                                                    {{--<button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar la categoria--}}
                                                {{--'{{ $producto->nombre }}' ?">--}}
                                                        {{--<span class="icon-bin"></span> Eliminar--}}
                                                    {{--</button>--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="col-md-12">
                                       <div class="row">
                                           <div class="col-md-6 text-center">
                                               <h1>{{ $producto->nombre }}</h1>
                                               <p>{{ $producto->codigo }}</p>
                                               <p class="font-weight-bold" style="margin-bottom: 0!important;">Stock disponible</p>
                                               <h1>{{ $producto->cantidadInventario() }} und.</h1>
                                               <p class="font-weight-bold " style="margin-bottom: 0!important;"> Precio venta</p>
                                               <p class="font-weight-bold text-success" style="font-size: 1.2em; !important">S/ {{ number_format($producto->precio, 2, ',', ' ') }}</p>

                                           </div>
                                           <div class="col-md-6 text-center">
                                               <br><br><br>
                                               <div class="btn-group btn-group-md">
                                                   <a href="{{ route('admin.inventario.agregar',$producto->id) }}" class="btn btn-success">
                                                       <span class="icon-enter"></span><br> Agregar Stock
                                                   </a>
                                                   <a href="{{ route('admin.inventario.sacar',$producto->id) }}" class="btn btn-secondary">
                                                       <span class="icon-exit"></span> <br> Eliminar Stock
                                                   </a>
                                               </div>
                                           </div>
                                       </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <form action="{{ $producto->url_eliminar }}" method="post" class=" inline">
                                            @csrf
                                            {!! method_field('DELETE') !!}
                                            <div class="btn-group btn-group-md">
                                                <a href="{{ $producto->url_editar}}" class="btn btn-warning">
                                                    <span class="icon-pencil2"></span> Editar
                                                </a>
                                                <button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar la categoria
                                                '{{ $producto->nombre }}' ?">
                                                    <span class="icon-bin"></span> Eliminar
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h3 class="text-center">Historial de Inventario</h3>
                                <table id="datatable_historial" class="table table-bordered dt-responsive nowrap">
                                    <thead>
                                    <tr>
                                        <th>Fecha/Hora Creación</th>
                                        <th># de Recibo</th>
                                        <th>Fecha</th>
                                        <th>Descripción</th>
                                        <th>Referencia</th>
                                        <th>Total</th>
                                        {{--<th>Opciones</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--@if(isset($listado_inventario) && count($listado_inventario)>0)--}}
                                        {{--@foreach($listado_inventario as $id=>$inventario)--}}
                                            {{--<tr class="{{ $inventario->tipo == 'ENTRADA' ? 'table-success' : 'table-danger' }}">--}}
                                                {{--<td>{{ $inventario->fecha }}</td>--}}
                                                {{--<td>{{ $inventario->hora }}</td>--}}
                                                {{--<td>{{ $inventario->detalle }}</td>--}}
                                                {{--<td>{{ $inventario->referencia }}</td>--}}
                                                {{--<td>{{ $inventario->cantidad }}</td>--}}
                                            {{--</tr>--}}
                                        {{--@endforeach--}}
                                    {{--@endif--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready( function () {
            $(".alert").fadeTo(500, 500).delay(5000).slideUp(500);
            $('#datatable_historial').dataTable({
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },
                serverSide: true,
                ajax: "{{ route('admin.productos.listado.ajax',$producto->id) }}",
                columns: [
                    {data: 'fecha_hora'},
                    {data: 'numero_recibo'},
                    {data: 'fecha_editable'},
                    {data: 'detalle'},
                    {data: 'referencia'},
                    {data: 'cantidad'},
                ],
                order: [[2, 'desc']],
                dom: 'lBfrtip',
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10', '25', '50', 'Todos' ]
                ],
                buttons: [{
                    extend: 'excel',
                    text: 'Exporta a Excel',
                    exportOptions:{
                        modifier : {
                            order : 'applied',  // 'current', 'applied', 'index',  'original'
                            page : 'all',      // 'all',     'current'
                            search : 'applied'     // 'none',    'applied', 'removed'
                        }
                    }
                }]
            });
        } );
    </script>
@endpush