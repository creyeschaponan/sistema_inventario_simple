@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.modal')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @include('layouts.notificacion')
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="{{ route('admin.inventario.listado') }}" class="btn btn-primary">Regresar</a>
                            </div>
                            <div class="col-md-10">
                                <h3>{{ $producto->id != null ? 'Editar' : 'Nuevo' }} Producto</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="" class="form-horizontal form-label-left" accept-charset="UTF-8" enctype="multipart/form-data">
                            @csrf
                            @if($producto->id != null)
                                {!!  method_field('PUT') !!}
                            @endif

                            <div class="form-group row ">
                                <label for="codigo" class="col-md-4 col-form-label text-md-right">Código <span class="required">*</span></label>

                                <div class="col-md-6">
                                    <input id="codigo" type="text" maxlength="30" class="form-control{{ $errors->has('codigo') ? ' is-invalid' : '' }} "
                                           name="codigo" value="{{ old('codigo',$producto->codigo) }}"
                                           data-validate-length-range="6" data-validate-words="2" autofocus>
                                    @if ($errors->has('codigo'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('codigo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre <span class="required">*</span></label>

                                <div class="col-md-6">
                                    <input id="nombre" type="text" maxlength="30" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }} "
                                           name="nombre" value="{{ old('nombre',$producto->nombre) }}"
                                           data-validate-length-range="6" data-validate-words="2" autofocus>
                                    @if ($errors->has('nombre'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('nombre') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="categoria_id" class="col-md-4 col-form-label text-md-right">Categoría <span class="required">*</span></label>

                                <div class="col-md-6">
                                    <select name="categoria_id" id="categoria_id" class="form-control show-tick
                                        {{ $errors->has('categoria_id') ? ' is-invalid' : '' }}">
                                        <option value="">Seleccione una Categoría</option>
                                        @if(isset($listado_categorias) && count($listado_categorias)>0)
                                            @foreach($listado_categorias as $id=>$categoria)
                                                <option value="{{ $categoria->id }}" {{($categoria->id == $producto->categoria_id) ? "selected":"" }}>{{ $categoria->nombre }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('categoria_id'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('categoria_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row ">
                                <label for="precio" class="col-md-4 col-form-label text-md-right">Precio <span class="required">*</span> <span class="font-weight-bold">S/</span></label>

                                <div class="col-md-6">
                                    <input id="precio" type="text" class="form-control{{ $errors->has('precio') ? ' is-invalid' : '' }} "
                                           name="precio" value="{{ old('precio',$producto->precio) }}"
                                           data-validate-length-range="6" data-validate-words="2" autofocus>
                                    @if ($errors->has('precio'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('precio') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            @if($producto->id != null)
                               @else
                                    <div class="form-group row ">
                                        <label for="stock" class="col-md-4 col-form-label text-md-right">Stock <span class="required">*</span></label>

                                        <div class="col-md-6">
                                            <input id="stock" type="number" class="form-control{{ $errors->has('stock') ? ' is-invalid' : '' }} "
                                                   name="stock" value="{{ old('stock',$producto->stock) }}"
                                                   placeholder="Stock Inicial" autofocus>
                                            @if ($errors->has('stock'))
                                                <span class="invalid-feedback text-danger">
                                                <strong>{{ $errors->first('stock') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                               @endif
                            {{--@if($producto->id != null)--}}
                            {{--@else--}}
                                {{--<div class="form-group row ">--}}
                                    {{--<label for="stock" class="col-md-4 col-form-label text-md-right">Imagen <span class="required">*</span></label>--}}

                                    {{--<div class="col-md-6">--}}
                                        {{--<input id="imagen" type="file" name="imagen" class="form-control{{ $errors->has('imagen') ? ' is-invalid' : '' }} ">--}}
                                        {{--@if ($errors->has('imagen'))--}}
                                            {{--<span class="invalid-feedback text-danger">--}}
                                            {{--<strong>{{ $errors->first('imagen') }}</strong>--}}
                                        {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endif--}}


                            <div class="form-group row">
                                <label for="estado" class="col-md-4 col-form-label text-md-right">Estado <span class="required">*</span></label>

                                <div class="col-md-6">
                                    <select name="estado" id="estado" class="form-control {{ $errors->has('estado') ? ' is-invalid' : '' }}">
                                        <option value="AC" {{$producto->estado == 'AC' ? 'selected' : '' }} >activo</option>
                                        <option value="IN" {{$producto->estado == 'IN' ? 'selected' : '' }}>inactivo</option>
                                    </select>

                                    @if ($errors->has('estado'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('estado') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-5">
                                    <button id="send" type="submit" class="btn btn-success">
                                        @if($producto->nombre != null)
                                            Actualizar
                                        @else
                                            Guardar
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection