@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.modal')
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('layouts.notificacion')
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Listado de Categorias</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{ route('admin.categoria.crear') }}" class="btn btn-primary">Nuevo</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="datatable_categorias" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombres / Usuario</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--@if(isset($listado_categorias) && count($listado_categorias)>0)--}}
                                {{--@foreach($listado_categorias as $id=>$categoria)--}}
                                    {{--<tr>--}}
                                        {{--<td>{{ $categoria->id }}</td>--}}
                                        {{--<td>{{ $categoria->user->name }} ({{ $categoria->user->username }})</td>--}}
                                        {{--<td>{{ $categoria->nombre }}</td>--}}
                                        {{--<td>{{ $categoria->descripcion }}</td>--}}
                                        {{--<td>--}}
                                            {{--@if($categoria->estado==$categoria::ESTADO_ACTIVO)--}}
                                                {{--<a href="#" class="btn btn-success btn-sm">ACTIVO</a>--}}
                                            {{--@elseif($categoria->estado==$categoria::ESTADO_INACTIVO)--}}
                                                {{--<a href="#" class="btn btn-danger btn-sm">INACTIVO</a>--}}
                                            {{--@endif--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                            {{--<form action="{{ $categoria->url_eliminar }}" method="post" class=" inline">--}}
                                                {{--@csrf--}}
                                                {{--{!! method_field('DELETE') !!}--}}
                                                {{--<div class="btn-group btn-group-sm">--}}
                                                    {{--<a href="{{ $categoria->url_editar}}" class="btn btn-warning">--}}
                                                        {{--<span class="icon-pencil2"></span> Editar--}}
                                                    {{--</a>--}}
                                                    {{--<button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar la categoria--}}
                                                {{--'{{ $categoria->nombre }}' ?">--}}
                                                        {{--<span class="icon-bin"></span> Eliminar--}}
                                                    {{--</button>--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</td>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                            {{--@endif--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready( function () {
            $(".alert").fadeTo(500, 500).delay(5000).slideUp(500);
            $('#datatable_categorias').dataTable({
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },
                serverSide: true,
                ajax: "{{ route('admin.categoria.listado.ajax') }}",
                columns: [
                    {data: 'id'},
                    {data: 'usuario'},
                    {data: 'nombre'},
                    {data: 'descripcion'},
                    {data: 'estado', name:'action', orderable:false ,searchable:false},
                    {data: 'opciones', name:'action', orderable:false ,searchable:false},
                ],
                order: [[0, 'desc']],
                dom: 'lBfrtip',
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10', '25', '50', 'Todos' ]
                ],
                buttons: [{
                    extend: 'excel',
                    text: 'Exporta a Excel',
                    exportOptions:{
                        modifier : {
                            order : 'applied',  // 'current', 'applied', 'index',  'original'
                            page : 'all',      // 'all',     'current'
                            search : 'applied'     // 'none',    'applied', 'removed'
                        }
                    }
                }]
            });
        } );
    </script>
@endpush
