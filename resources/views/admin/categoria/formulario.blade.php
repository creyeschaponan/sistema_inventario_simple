@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.modal')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @include('layouts.notificacion')
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="{{ route('admin.categoria.listado') }}" class="btn btn-primary">Regresar</a>
                            </div>
                            <div class="col-md-10">
                                <h3>{{ $categoria->id != null ? 'Editar' : 'Nueva' }} Categoría</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                            <form method="POST" action="" >
                                @csrf
                                @if($categoria->id != null)
                                    {!!  method_field('PUT') !!}
                                @endif

                                <div class="form-group row ">
                                    <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre <span class="required">*</span></label>

                                    <div class="col-md-6">
                                        <input id="nombre" type="text" maxlength="30" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }} "
                                               name="nombre" value="{{ old('nombre',$categoria->nombre) }}"
                                               data-validate-length-range="6" data-validate-words="2" autofocus>
                                        @if ($errors->has('nombre'))
                                            <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('nombre') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <label for="descripcion" class="col-md-4 col-form-label text-md-right">Descripción <span class="required">*</span></label>
                                    <div class="col-md-6">
                                    <textarea name="descripcion" id="descripcion"
                                              class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }} " cols="30" rows="4">{{ old('descripcion',$categoria->descripcion) }}</textarea>
                                        @if ($errors->has('descripcion'))
                                            <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('descripcion') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="estado" class="col-md-4 col-form-label text-md-right">Estado <span class="required">*</span></label>

                                    <div class="col-md-6">
                                        <select name="estado" id="estado" class="form-control {{ $errors->has('estado') ? ' is-invalid' : '' }}">
                                            <option value="AC" {{$categoria->estado == 'AC' ? 'selected' : '' }} >activo</option>
                                            <option value="IN" {{$categoria->estado == 'IN' ? 'selected' : '' }}>inactivo</option>
                                        </select>

                                        @if ($errors->has('estado'))
                                            <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('estado') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button id="send" type="submit" class="btn btn-success">
                                            @if($categoria->nombre != null)
                                                Actualizar
                                            @else
                                                Guardar
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
