@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.modal')
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('layouts.notificacion')
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Listado de Usuarios</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{ route('admin.usuarios.crear') }}" class="btn btn-primary">Nuevo</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre Persona</th>
                                <th>Usuario</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($listado_usuarios) && count($listado_usuarios)>0)
                                @foreach($listado_usuarios as $id=>$usuario)
                                    <tr>
                                        <td>{{ $usuario->id }}</td>
                                        <td>{{ $usuario->name }}</td>
                                        <td>{{ $usuario->username }}</td>
                                        <td>
                                            @if($usuario->estado==$usuario::ESTADO_ACTIVO)
                                                <a href="#" class="btn btn-success btn-sm">ACTIVO</a>
                                            @elseif($usuario->estado==$usuario::ESTADO_INACTIVO)
                                                <a href="#" class="btn btn-danger btn-sm">INACTIVO</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($usuario->id != 1)
                                                <form action="{{ $usuario->url_eliminar }}" method="post" class=" inline">
                                                    @csrf
                                                    {!! method_field('DELETE') !!}
                                                    <div class="btn-group btn-group-sm">
                                                        <a href="{{ $usuario->url_editar}}" class="btn btn-warning">
                                                            <span class="icon-pencil2"></span> Editar
                                                        </a>
                                                        <button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar la categoria
                                                '{{ $usuario->name }}' ?">
                                                            <span class="icon-bin"></span> Eliminar
                                                        </button>
                                                    </div>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
