@extends('layouts.app')
@section('content')
    <div class="container">
        @include('layouts.modal')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @include('layouts.notificacion')
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="{{ route('admin.usuarios.listado') }}" class="btn btn-primary">Regresar</a>
                            </div>
                            <div class="col-md-10">
                                <h3>{{ $usuario->id != null ? 'Editar' : 'Nueva' }} Usuario</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="" >
                            @csrf
                            @if($usuario->id != null)
                                {!!  method_field('PUT') !!}
                            @endif

                            <div class="form-group row ">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Nombre <span class="required">*</span></label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} "
                                           name="name" value="{{ old('nombre',$usuario->name) }}"
                                            autofocus>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="username" class="col-md-4 col-form-label text-md-right">Usuario <span class="required">*</span></label>

                                <div class="col-md-6">
                                    <input id="username" type="text" maxlength="30" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }} "
                                           name="username" value="{{ old('username',$usuario->username) }}"
                                            autofocus>
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="item form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña *</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" value="{{ old('password') }}">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="item form-group row">
                                <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">Confirmar contraseña *</label>
                                <div class="col-md-6">
                                    <input id="password_confirmation" type="password" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="password_confirmation" value="{{ old('password_confirmation') }}">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="estado" class="col-md-4 col-form-label text-md-right">Estado <span class="required">*</span></label>

                                <div class="col-md-6">
                                    <select name="estado" id="estado" class="form-control {{ $errors->has('estado') ? ' is-invalid' : '' }}">
                                        <option value="AC" {{$usuario->estado == 'AC' ? 'selected' : '' }} >activo</option>
                                        <option value="IN" {{$usuario->estado == 'IN' ? 'selected' : '' }}>inactivo</option>
                                    </select>

                                    @if ($errors->has('estado'))
                                        <span class="invalid-feedback text-danger">
                                            <strong>{{ $errors->first('estado') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button id="send" type="submit" class="btn btn-success">
                                        @if($usuario->nombre != null)
                                            Actualizar
                                        @else
                                            Guardar
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
