<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth:web','prefix'=>'admin'],function(){

    Route::group(['prefix' => 'categorias'],function (){
        Route::get('/','CategoriaController@listado')->name('admin.categoria.listado');
        Route::get('/crear','CategoriaController@crear')->name('admin.categoria.crear');
        Route::post('/crear','CategoriaController@almacenar');
        Route::get('/{id}/editar','CategoriaController@editar')->name('admin.categoria.editar');
        Route::put('/{id}/editar','CategoriaController@actualizar');
        Route::delete('/{id}/eliminar','CategoriaController@eliminar')->name('admin.categoria.eliminar');

        Route::get('/ajax','CategoriaController@listadoAjax')->name('admin.categoria.listado.ajax');
    });

    Route::group(['prefix' => 'inventario'],function (){
        Route::get('/','InventarioController@listado')->name('admin.inventario.listado');

        //id_producto
        Route::get('/{id}/agregar','InventarioController@agregar')->name('admin.inventario.agregar');
        Route::post('/{id}/agregar','InventarioController@almacenar');

        Route::get('/{id}/sacar','InventarioController@sacar')->name('admin.inventario.sacar');
        Route::post('/{id}/sacar','InventarioController@actualizar');

        //Route::delete('/{id}/eliminar','InventarioController@eliminar')->name('admin.inventario.eliminar');
    });

    Route::group(['prefix' => 'productos'],function (){
        Route::get('/','ProductoController@listado')->name('admin.productos.listado');
        Route::get('/crear','ProductoController@crear')->name('admin.productos.crear');
        Route::post('/crear','ProductoController@almacenar');

        Route::get('/{id}/detalle','ProductoController@detalle')->name('admin.productos.detalle');
        Route::post('/{id}/cambiar/imagen','ProductoController@cambiarImagen')->name('admin.producto.cambiar.imagen');

        Route::get('/{id}/restaurar','ProductoController@restaurar')->name('admin.producto.restaurar');

        Route::get('/{id}/editar','ProductoController@editar')->name('admin.productos.editar');
        Route::put('/{id}/editar','ProductoController@actualizar');

        Route::delete('/{id}/eliminar','ProductoController@eliminar')->name('admin.productos.eliminar');

        Route::get('/{id}/listado/ajax','ProductoController@listadoAjax')->name('admin.productos.listado.ajax');

    });

    Route::group(['prefix' => 'reportes'],function(){
        Route::get('/','ReporteController@listado')->name('admin.reportes.listado');

        Route::get('/listado/ajax','ReporteController@listadoAjax')->name('admin.reportes.listado.ajax');
    });

    Route::group(['prefix' => 'usuarios'],function (){
        Route::get('/','UsuariosController@listado')->name('admin.usuarios.listado');
        Route::get('/crear','UsuariosController@crear')->name('admin.usuarios.crear');
        Route::post('/crear','UsuariosController@almacenar');
        Route::get('/{id}/editar','UsuariosController@editar')->name('admin.usuarios.editar');
        Route::put('/{id}/editar','UsuariosController@actualizar');
        Route::delete('/{id}/eliminar','UsuariosController@eliminar')->name('admin.usuarios.eliminar');
    });


});